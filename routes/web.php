<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('generate-pdf', [PDFController::class, 'generatePDF']);


Auth::routes(['verify' => true]);

Route::group(['middleware' => ['verified']], function () {
Route::resource('customers' , \App\Http\Controllers\CustomerController::class);
Route::resource('users' , \App\Http\Controllers\UserController::class);
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
